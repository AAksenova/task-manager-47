
CREATE TABLE IF NOT EXISTS public.tm_project
(
    id character varying(50) COLLATE pg_catalog."default" NOT NULL,
    created timestamp without time zone NOT NULL,
    name character varying(100) COLLATE pg_catalog."default" NOT NULL,
    description character varying(255) COLLATE pg_catalog."default",
    status character varying(30) COLLATE pg_catalog."default",
    user_id character varying(50) COLLATE pg_catalog."default",
    CONSTRAINT tm_project_pkey PRIMARY KEY (id)
)

TABLESPACE pg_default;

ALTER TABLE IF EXISTS public.tm_project
    OWNER to postgres;

CREATE TABLE IF NOT EXISTS public.tm_session
(
    id character varying(50) COLLATE pg_catalog."default" NOT NULL,
    user_id character varying(50) COLLATE pg_catalog."default" NOT NULL,
    date timestamp with time zone,
    role character varying(30) COLLATE pg_catalog."default",
    CONSTRAINT tm_session_pkey PRIMARY KEY (id)
)

TABLESPACE pg_default;

ALTER TABLE IF EXISTS public.tm_session
    OWNER to postgres;

CREATE TABLE IF NOT EXISTS public.tm_task
(
    id character varying(50) COLLATE pg_catalog."default" NOT NULL,
    created timestamp without time zone NOT NULL,
    name character varying(100) COLLATE pg_catalog."default" NOT NULL,
    description character varying(255) COLLATE pg_catalog."default",
    status character varying(30) COLLATE pg_catalog."default",
    user_id character varying(50) COLLATE pg_catalog."default",
    project_id character varying(50) COLLATE pg_catalog."default",

    CONSTRAINT tm_task_pkey PRIMARY KEY (id)
)

TABLESPACE pg_default;

ALTER TABLE IF EXISTS public.tm_task
    OWNER to postgres;

CREATE TABLE IF NOT EXISTS public.tm_user
(
    id character varying(50) COLLATE pg_catalog."default" NOT NULL,
    login character varying(50) COLLATE pg_catalog."default" NOT NULL,
    password_hash character varying(100) COLLATE pg_catalog."default" NOT NULL,
    email character varying(50) COLLATE pg_catalog."default",
    first_name character varying(100) COLLATE pg_catalog."default",
    last_name character varying(100) COLLATE pg_catalog."default",
    middle_name character varying(100) COLLATE pg_catalog."default",
    role character varying(50) COLLATE pg_catalog."default" NOT NULL,
    locked boolean,
    CONSTRAINT tm_user_pkey PRIMARY KEY (id)
)

TABLESPACE pg_default;

ALTER TABLE IF EXISTS public.tm_user
    OWNER to postgres;
