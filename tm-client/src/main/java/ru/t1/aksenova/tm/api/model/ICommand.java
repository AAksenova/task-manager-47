package ru.t1.aksenova.tm.api.model;

import org.jetbrains.annotations.Nullable;
import ru.t1.aksenova.tm.enumerated.Role;

public interface ICommand {

    @Nullable
    String getArgument();

    @Nullable
    String getDescription();

    @Nullable
    String getName();

    void execute();

    @Nullable
    Role[] getRoles();

}
