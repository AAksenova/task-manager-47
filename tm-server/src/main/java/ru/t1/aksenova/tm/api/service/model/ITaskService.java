package ru.t1.aksenova.tm.api.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.aksenova.tm.api.repository.model.ITaskRepository;
import ru.t1.aksenova.tm.enumerated.Status;
import ru.t1.aksenova.tm.enumerated.TaskSort;
import ru.t1.aksenova.tm.model.Task;

import javax.persistence.EntityManager;
import java.util.Comparator;
import java.util.List;

public interface ITaskService extends IUserOwnedService<Task> {

    @NotNull
    ITaskRepository getRepository(@NotNull EntityManager entityManager);

    @NotNull
    Task create(
            @Nullable String userId,
            @Nullable String name,
            @Nullable String description
    );

    @NotNull
    List<Task> findAll();

    @NotNull
    List<Task> findAll(@Nullable String userId);

    @NotNull
    List<Task> findAll(@Nullable String userId, @Nullable TaskSort sort);

    @NotNull
    List<Task> findAll(
            @Nullable String userId,
            @Nullable Comparator<Task> comparator
    );

    @NotNull
    List<Task> findAllByProjectId(
            @Nullable String userId,
            @Nullable String projectId
    );

    @Nullable
    Task findOneById(
            @Nullable String userId,
            @Nullable String id
    );

    void clear();

    void removeAll(@Nullable String userId);

    @Nullable
    Task removeOne(
            @Nullable String userId,
            @Nullable Task task
    );

    @Nullable
    Task removeOneById(
            @Nullable String userId,
            @Nullable String id
    );

    @NotNull
    Task updateById(
            @Nullable String userId,
            @Nullable String id,
            @Nullable String name,
            @Nullable String description
    );

    @NotNull
    Task changeTaskStatusById(
            @Nullable String userId,
            @Nullable String id,
            @Nullable Status status
    );

    boolean existsById(@Nullable String userId, @Nullable String id);

    long getSize(@Nullable String userId);
}
